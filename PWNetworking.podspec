

Pod::Spec.new do |s|

  s.name         = "PWNetworking"
  s.version      = "0.0.1"
  s.summary      = "A short description of PWNetworking."

  s.description  = <<-DESC
  					PWNetworking
                   DESC

  s.homepage     = "git@gitlab.com:devpublic/PWNetworking.git"

  s.license      = "MIT"

  s.author             = { "liuchong" => "306fengdai@gmail.com" }

  s.platform     = :ios, "8.0"

  s.ios.deployment_target = "8.0"
  s.source       = { :git => "git@gitlab.com:devpublic/PWNetworking.git", :tag => "{s.version}" }
  s.source_files  = "PWNetworking/**/*.{h,m}"
  s.dependency "AFNetworking"


end
