
#import "PWHTTPSessionManager.h"

@implementation PWHTTPSessionManager

/**
 *  单例模式
 *
 *  @return 单例对象
 */
+ (instancetype)defaultManager {

    static PWHTTPSessionManager *_defaultManager = nil;
    
    if (!_defaultManager) {
        
        _defaultManager = [PWHTTPSessionManager new];
        
        // 默认连接正式环境
        [_defaultManager setEnvironmentType:PWEnvironmentTypeProduction];
    }
    
    return _defaultManager;
}


- (void)setEnvironmentType:(PWEnvironmentType)environmentType {

    NSString *_baseURL = @"";

    _environmentType = environmentType;
    
    switch (environmentType) {
        case PWEnvironmentTypeProduction:
            
            _baseURL = @"https://pw.360fengdai.com";
            //...
            break;
        //...
        default: //默认生产环境
            _baseURL = @"https://pw.360fengdai.com";
            //...
            break;
    }
    
    [self setSessionManagerWithBaseUrl:_baseURL];
    
}

- (void)setSessionManagerWithBaseUrl:(NSString *)baseUrl{
    
    self.fengdaiSessionManager = [[PWCustomHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:baseUrl]];
    self.fengdaiSessionManager.requestSerializer.timeoutInterval = 30;
    self.fengdaiSessionManager.requestSerializer = [AFJSONRequestSerializer serializer];
    self.fengdaiSessionManager.responseSerializer = [AFJSONResponseSerializer serializer];
}


@end

// 拼接URL
NSString *jointUrl(NSString *prefixUrl, NSString *path) {
    
    NSString *urlString = [NSString stringWithFormat:@"%@/%@", prefixUrl, path];
    return urlString;
};


