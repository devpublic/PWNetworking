
#import <AFNetworking/AFNetworking.h>
#import "PWCustomHTTPSessionManager.h"

/**
 *  连接的服务器环境类型
 */
typedef NS_ENUM(NSUInteger, PWEnvironmentType) {
    
    /**
     *  开发环境
     */
    PWEnvironmentTypeDevelopment,
    
    /**
     *  测试环境
     */
    PWEnvironmentTypeTest,
    /**
     *  生产环境
     */
    PWEnvironmentTypeProduction,
    
    /**
     *  测试环境_外网
     */
    PWEnvironmentTypeTestOnLine,
    
    /**
     * 测试云
     */
    PWEnvironmentTypeTestOnAliCloud,
    /**
     *预生产
     */
    PWEnvironmentTypePreProduction,
    /**
     *  3.0系统
     */
    PWEnvironmentTypeTest3_0,
    
    /** Test3.0 online */
    PWEnvironmentTypeTestOnline3_0,
};

@interface PWHTTPSessionManager : NSObject

/**
 * defaultSessionManager
 */
@property (nonatomic, strong) PWCustomHTTPSessionManager*fengdaiSessionManager;

/**
 *  服务器环境类型
 */
@property (nonatomic, assign) PWEnvironmentType environmentType;

/**
 *  单例模式
 *
 *  @return 单例对象
 */
+ (instancetype)defaultManager;

- (void)setSessionManagerWithBaseUrl:(NSString *)baseUrl;

//@property (nonatomic, copy) NSString *prefixUrl;

//微信端地址前缀
//@property (nonatomic, copy) NSString *wxPrefixUrl;

// 跳转到登录页面
//@property (nonatomic, copy) NSString *loginUrl;
//...

@end

NSString *jointUrl(NSString *prefixUrl, NSString *path);
