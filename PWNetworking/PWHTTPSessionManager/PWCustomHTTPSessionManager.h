//
//  PWUpLoadImgeHTTPSessionManager.h
//  PocketWallet
//
//  Created by liuchong on 2017/8/15.
//

#import <AFNetworking/AFNetworking.h>

@interface PWCustomHTTPSessionManager : AFHTTPSessionManager

- (NSURLSessionDataTask *_Nullable)CustomPOST:(NSString *_Nullable)URLString
                                   parameters:(id _Nullable )parameters
                    constructingBodyWithBlock:(void (^_Nullable)(_Nullable id <AFMultipartFormData> formData))block
                      progress:(nullable void (^)(NSProgress * _Nonnull))uploadProgress
                                      success:(void (^_Nullable)(NSURLSessionDataTask * _Nullable task, id _Nullable responseObject))success
                                      failure:(void (^_Nullable)(NSURLSessionDataTask * _Nullable task, NSError * _Nullable error, id _Nullable responseObject))failure;


@end
