#import <Foundation/Foundation.h>
#import "PWResult.h"

typedef void (^PWRequestFinishedBlock)(PWResult *result);

/**
 *  连接服务系统的类型
 */
typedef NS_ENUM(NSUInteger, PWSessionManagerType) {
    
    /**
     *  蜂泰系统
     */
    PWSessionManagerTypeFengDai,
};



typedef NS_ENUM(NSUInteger, PWHTTPContentType) {
    
    PWHTTPContentTypeJSON,
    
    PWHTTPContentTypeURLEncoded,
};


/**
 *  HTTP请求类型
 */
typedef NS_ENUM(NSUInteger, PWHTTPRequestType){
    /**
     *  无类型
     */
    PWHTTPRequestTypeNone,
    
    /**
     *  返回服务器针对特定资源所支持的HTTP请求方法。也可以利用向Web服务器发送'*'的请求来测试服务器的功能性。
     */
    PWHTTPRequestTypeOPTIONS,
    
    /**
     *  向服务器索要与GET请求相一致的响应，只不过响应体将不会被返回。这一方法可以在不必传输整个响应内容的情况下，就可以获取包含在响应消息头中的元信息。
     */
    PWHTTPRequestTypeHEAD,
    
    /**
     *  向特定的资源发出请求。
     */
    PWHTTPRequestTypeGET,
    
    
    /**
     *  向指定资源提交数据进行处理请求（例如提交表单或者上传文件）。数据被包含在请求体中。POST请求可能会导致新的资源的创建和/或已有资源的修改。
     */
    PWHTTPRequestTypePOST,
    
    
    /**
     *  向指定资源位置上传其最新内容。
     */
    PWHTTPRequestTypePUT,
    
    
    /**
     *  请求服务器删除Request-URI所标识的资源。
     */
    PWHTTPRequestTypeDELETE,
    
    
    /**
     *  回显服务器收到的请求，主要用于测试或诊断。
     */
    PWHTTPRequestTypeTRACE,
    
    
    /**
     *  HTTP/1.1协议中预留给能够将连接改为管道方式的代理服务器。
     */
    PWHTTPRequestTypeCONNECT,
};



typedef NS_ENUM(NSInteger, PWStatusCodeType) {
    
    
    /**
     *  请求成功
     */
    PWStatusCodeTypeRequsetSuccess = 200,
    
    
    /**
     *  请求出错
     */
    PWStatusCodeTypeRequsetFailure = 400,
    
    
    /**
     *  Session超时
     */
    PWStatusCodeTypeSessionTimeOut = 401,
    
    
    /**
     *  服务器出错
     */
    PWStatusCodeTypeServerError = 500,
};

@interface PWRequest : NSObject

//typedef void (^PWRequestFinishedBlock)(PWRequest *result);

/**
 *  当前正在执行的操作
 */
@property (nonatomic, weak) NSURLSessionDataTask *task;


/**
 *   HTTP请求的类型
 */
@property (nonatomic, assign) PWHTTPRequestType requestType;


/**
 *  请求的序列化类型
 */
@property (nonatomic, assign) PWHTTPContentType contentType;


/**
 *  使用POST请求，不处理错误回调
 */
- (void)startWithParams:(id)params successBlock:(PWRequestFinishedBlock)successBlock;


/**
 *  使用的请求方法
 */
- (void)startWithSessionManagerType:(PWSessionManagerType)sessionManagerType requestType:(PWHTTPRequestType)requestType params:(id)params successBlock:(PWRequestFinishedBlock)successBlock failureBlock:(PWRequestFinishedBlock)failureBlock;




/**
 *  默认使用的请求方法，使用POST请求
 */
- (void)startWithParams:(id)params successBlock:(PWRequestFinishedBlock)successBlock failureBlock:(PWRequestFinishedBlock)failureBlock;


/**
 上传图片方法 默认POST
 */
- (void)uploadWithURLString:(NSString *)urlString
                     images:(NSDictionary *)images
                     Params:(id)params
              progressBlock:(void(^)(NSProgress *progress))progressBlock
               successBlock:(PWRequestFinishedBlock)successBlock
               failureBlock:(PWRequestFinishedBlock)failureBlock;
/**
 *  取消当前正在执行的请求
 */
- (void)cancel;

@end
