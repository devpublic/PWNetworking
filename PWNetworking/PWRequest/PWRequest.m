#import "PWRequest.h"
#import "PWHTTPSessionManager.h"
#import "PWCustomHTTPSessionManager.h"

@interface PWRequest () {
    
    NSString *requestURL;
}


/**
 *  网络请求结束后请求结果的分析
 *
 *  @param params         传给服务端的参数
 *  @param responseObject 服务端返回的数据
 *  @param error          网络请求失败的错误信息
 *  @param successBlock   接口获取数据成功的回调
 *  @param failureBlock   接口获取数据失败的回调
 */
- (void)requestFinishedWithParams:(NSDictionary *)params responseObject:(id)responseObject error:(NSError *)error SuccessBlock:(PWRequestFinishedBlock)successBlock failureBlock:(PWRequestFinishedBlock)failureBlock;

@end


@implementation PWRequest

#pragma mark - Public Methods
#pragma mark  使用POST请求，不处理错误回调
- (void)startWithParams:(id)params successBlock:(PWRequestFinishedBlock)successBlock {
    
    
}


#pragma mark 默认使用的请求方法，使用POST请求
- (void)startWithParams:(id)params successBlock:(PWRequestFinishedBlock)successBlock failureBlock:(PWRequestFinishedBlock)failureBlock {
    
    [self startWithSessionManagerType:PWSessionManagerTypeFengDai requestType:_requestType params:params successBlock:successBlock failureBlock:failureBlock];
}



#pragma mark 默认使用的请求方法，使用POST请求
- (void)startWithSessionManagerType:(PWSessionManagerType)sessionManagerType requestType:(PWHTTPRequestType)requestType params:(id)params successBlock:(PWRequestFinishedBlock)successBlock failureBlock:(PWRequestFinishedBlock)failureBlock {
    
    if (self.task) {
        
        [self cancel];
    }
    
    AFHTTPSessionManager *manager = nil;
    
    switch (sessionManagerType) {
            
            case PWSessionManagerTypeFengDai: {
                
                manager = [PWHTTPSessionManager defaultManager].fengdaiSessionManager;
                break;
            }
            
        default:
            break;
    }
    
    requestURL = params[@"url"];
    
    if (!manager || !requestURL) {
        
        if (!manager) {
            
            NSLog(@"未配置服务系统");
        }
        
        if (!requestURL) {
            
            NSLog(@"未配置请求地址");
        }
        
        return;
    }
    
    // 设置请求序列化方式
    switch (self.contentType) {
            
        case PWHTTPContentTypeJSON: {
            
            manager.requestSerializer = [AFJSONRequestSerializer serializer];
            break;
        }
            
        case PWHTTPContentTypeURLEncoded: {
            manager.requestSerializer = [AFHTTPRequestSerializer serializer];
            break;
        }
            
        default:
            break;
    }
    
    // 设置请求时间
    manager.requestSerializer.timeoutInterval = 30;
    if (self.requestType == PWHTTPRequestTypeNone) {
        
        self.requestType = PWHTTPRequestTypePOST;
    }
    
    
    NSString *methodName = nil;
    
    switch (self.requestType) {
            
        case PWHTTPRequestTypeNone: {
            
            break;
        }
            
        case PWHTTPRequestTypeOPTIONS: {
            
            methodName = @"OPTIONS";
            break;
        }
            
        case PWHTTPRequestTypeHEAD: {
            
            methodName = @"HEAD";
            break;
        }
            
            
        case PWHTTPRequestTypeGET: {
            
            methodName = @"GET";
            
            break;
        }
            
            
        case PWHTTPRequestTypePOST: {
            
            methodName = @"POST";
        }
            break;
            
        case PWHTTPRequestTypePUT: {
            
            methodName = @"PUT";
        }
            break;
            
        case PWHTTPRequestTypeDELETE: {
            
            methodName = @"DELETE";
        }
            break;
            
        case PWHTTPRequestTypeTRACE: {
            
            methodName = @"TRACE";
            break;
        }
            
        case PWHTTPRequestTypeCONNECT: {
            
            methodName = @"CONNECT";
            break;
        }
            
        default:
            break;
    }
    
    
    if (methodName.length > 0) {
        
        NSError *serializationError = nil;
        
        NSString *baseURL = [manager.baseURL absoluteString];
        
        NSString *urlString = nil;
        
        if (baseURL.length == 0) {
            
            urlString = requestURL;
            
        } else {
            
            if ([baseURL hasSuffix:@"/"] || [requestURL hasSuffix:@"/"]) {
                
                urlString = [NSString stringWithFormat:@"%@%@", baseURL, requestURL];
                
            } else {
                
                urlString = [NSString stringWithFormat:@"%@/%@", baseURL, requestURL];
            }
        }
        
        // 移除url参数
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        
        [params enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL * stop) {
            
            if (![key isEqualToString:@"url"]) {
                
                [parameters setValue:obj forKey:key];
            }
        }];
        
        
        NSMutableURLRequest *request = [manager.requestSerializer requestWithMethod:methodName URLString:urlString parameters:parameters error:&serializationError];
        
        
        [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
        
        //新方法
        self.task = [manager dataTaskWithRequest:request uploadProgress:^(NSProgress * _Nonnull uploadProgress) {
            
        } downloadProgress:^(NSProgress * _Nonnull downloadProgress) {
            
        } completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            
            [self requestFinishedWithParams:parameters responseObject:responseObject error:error SuccessBlock:successBlock failureBlock:failureBlock];
        }];
        [self.task resume];
    }
    
}

- (void)uploadWithURLString:(NSString *)urlString
                  images:(NSDictionary *)images
                     Params:(id)params
              progressBlock:(void(^)(NSProgress *progress))progressBlock
               successBlock:(PWRequestFinishedBlock)successBlock
               failureBlock:(PWRequestFinishedBlock)failureBlock {
    
    if (self.task) {
        
        [self cancel];
    }
    
    PWHTTPSessionManager *manager = [PWHTTPSessionManager defaultManager];

    manager.fengdaiSessionManager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    [manager.fengdaiSessionManager.requestSerializer setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    
    manager.fengdaiSessionManager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/plain",@"text/html", nil];
    
    //设置超时 和token
    manager.fengdaiSessionManager.requestSerializer.timeoutInterval = 30;
    
    self.task = [manager.fengdaiSessionManager CustomPOST:urlString parameters:params constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {

        if (images) {
            NSArray *allKeys = [images allKeys];
            [allKeys enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                NSString *imageKey = obj;
                NSData *imageData = [images valueForKey:imageKey];
                [formData appendPartWithFileData:imageData name:imageKey fileName:imageKey mimeType:@"image/jpeg"];
            }];
        }
        
    } progress:^(NSProgress * _Nonnull uploadProgress) {
        if (progressBlock) {
            progressBlock(uploadProgress);
        }
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        [self requestFinishedWithParams:params responseObject:responseObject error:nil SuccessBlock:successBlock failureBlock:failureBlock];
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error , id responseObject) {
        [self requestFinishedWithParams:params responseObject:responseObject error:error SuccessBlock:successBlock failureBlock:failureBlock];
        
    }];
}


#pragma mark 取消当前正在执行的请求
- (void)cancel {
    
    if (self.task) {
        
        [self.task cancel];
    }
    
    self.task = nil;
}


#pragma mark - Private Methods
#pragma mark 网络请求结束后请求结果的分析
- (void)requestFinishedWithParams:(NSDictionary *)params responseObject:(id)responseObject error:(NSError *)error SuccessBlock:(PWRequestFinishedBlock)successBlock failureBlock:(PWRequestFinishedBlock)failureBlock {
    
    NSLog(@"\n====================== REQUEST ======================\n\n%@\n\n%@\n\n", self.task.originalRequest.URL.absoluteString, params);

    NSInteger responseCode  = error.code;
    NSHTTPURLResponse * response = (NSHTTPURLResponse*)self.task.response;
    NSInteger statusCode = response.statusCode;
    
    NSLog(@"====================== RESPONSE STATUECODE ======================\n\n%d\n\n", (int)statusCode);
    if (statusCode == PWStatusCodeTypeRequsetSuccess) {
        
        NSLog(@"====================== RESPONSE SUCCESS ======================\n\n%@\n\n", responseObject);
        
        
        PWResult *result = [[PWResult alloc] init];
        
        // Response Header
        result.responseHeaderFields = response.allHeaderFields;
        
        // 服务端返回的状态码
        result.statusCode = response.statusCode;
        
        // 请求操作相应码
        result.responseCode = responseCode;
        
        // 服务端返回的数据
        result.output = responseObject;
        
        successBlock(result);
        
        return;
        
    } else if (statusCode >= PWStatusCodeTypeRequsetFailure && statusCode < PWStatusCodeTypeServerError) {
        
        // 处理 400 - 499的错误
        
        // 网络请求失败
        NSLog(@"====================== RESPONSE FAILURE ======================\n\n%d\n\n", (int)statusCode);
        NSLog(@"%@", responseObject);
        
        // 处理401
        if (statusCode == PWStatusCodeTypeSessionTimeOut) {

            PWResult * result = [PWResult new];
            result.responseHeaderFields = response.allHeaderFields;
            
            result.responseContent = @"您登录已超时，请重新登录";
            result.statusCode = statusCode;
            result.responseCode = responseCode;
            
            if ([[responseObject objectForKey:@"error"] isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *error = [responseObject objectForKey:@"error"];
                NSString *description = [error objectForKey:@"description"];
                
                if (![description isKindOfClass:[NSNull class]]) {
                    
                    if (!description||description==nil||description.length<=0) {
                        
                    }else{
                        
                        result.responseContent = description;
                    }
                }
                
                
            }
            
            failureBlock(result);
            
        } else {
            
            // 处理其他情况
            if ([[responseObject objectForKey:@"error"] isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *error = [responseObject objectForKey:@"error"];
                NSString *description = [error objectForKey:@"description"];
                NSInteger code = [[error objectForKey:@"code"] integerValue];
                
                
                PWResult *result = [PWResult new];
                
                // Response Header
                result.responseHeaderFields = response.allHeaderFields;
                
                // 服务端返回的状态码
                result.statusCode = response.statusCode;
                
                // 请求操作相应码
                result.responseCode = responseCode;
                
                // 服务端返回的数据
                result.output = responseObject;
                
                result.responseCode = code;
                result.responseContent = description;
                
                if ([result.responseContent length] == 0) {
                    
                    result.responseContent = @"服务器繁忙，请稍后再试";
                }
                
                failureBlock(result);
                
                return;
            }else{
                
                PWResult * result = [PWResult new];
                result.responseHeaderFields = response.allHeaderFields;
                result.responseContent = @"服务器繁忙，请稍后再试";
                failureBlock(result);
            }
        }
        
    } else if (statusCode >= PWStatusCodeTypeServerError && statusCode < 600) {
        
        // 服务器出错
        NSLog(@"====================== RESPONSE FAILURE ======================\n\n%d\n\n", (int)statusCode);
        NSLog(@"%@", responseObject);
        
        PWResult * result = [PWResult new];
        result.responseHeaderFields = response.allHeaderFields;
        result.responseContent = @"服务器繁忙，请稍后再试";
        failureBlock(result);
        
    } else  {
        
        // 网络请求失败
        NSLog(@"====================== RESPONSE FAILURE ======================\n%d\n%d\n", (int)error.code, (int)statusCode);
        NSLog(@"%@", responseObject);
        
        if (error) {
//            CFNetworkErrors
            switch (responseCode) {
                    
                case kCFURLErrorTimedOut:
                case kCFURLErrorBadServerResponse:
                case kCFURLErrorCannotConnectToHost:
                case kCFURLErrorNotConnectedToInternet: {
                    
                    //网络无法连接
                    
                    PWResult *result = [PWResult new];
                    result.responseCode = responseCode;
                    result.responseContent = @"网络异常，请稍后再试";
                    failureBlock(result);
                    
                    
                    break;
                }
                    
                case kCFURLErrorCancelled: {
                    // 用户手动取消不做任何处理
                    return;
                    break;
                }
                    
                case kCFURLErrorAppTransportSecurityRequiresSecureConnection: {
                    
                    
                    PWResult *result = [PWResult new];
                    result.responseCode = responseCode;
                    result.responseContent = @"请使用安全的网络请求方式";
                    failureBlock(result);
                    
                    
                    break;
                }
                    
                default: {
                    
                    
                    PWResult *result = [PWResult new];
                    result.responseCode = responseCode;
                    result.responseContent = @"服务器繁忙，请稍后再试";
                    failureBlock(result);
                    
                    
                    break;
                }
            }
            
        } else {
            
            
            PWResult *result = [PWResult new];
            result.responseHeaderFields = response.allHeaderFields;
            result.responseContent = @"服务器繁忙，请稍后再试";
            failureBlock(result);
            
        }
    }
}

@end
