
//  Created by liuchong on 2018/4/24.
//  Copyright © 2018年 liuchong. All rights reserved.
//

#import "ViewController.h"

#import "PWRequest.h"

#import "PWHTTPSessionManager.h"


@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    [ViewController getAppMinimumVersion:^(NSDictionary *output) {
        NSLog(@"%@",output);
    } failureBlock:^(PWResult *result) {
        NSLog(@"%@",result.responseContent);
    }];
}

+ (PWRequest *)getAppMinimumVersion:(void(^)(NSDictionary *output))successBlock
                       failureBlock:(PWRequestFinishedBlock)failureBlock {
    
    PWRequest * request = [[PWRequest alloc] init];
    request.requestType = PWHTTPRequestTypeGET;
    request.contentType = PWHTTPContentTypeJSON;
    
    NSDictionary *infoDictionary = [[NSBundle mainBundle] infoDictionary];
    NSNumber *build = [infoDictionary objectForKey:@"CFBundleVersion"];
    
    NSDictionary * params = @{@"url":@"SystemWeb/about/version",
                              @"versionCode": build,
                              @"deviceType":@"ios"};
    
    [request startWithParams:params successBlock:^(PWResult *result) {
        
        successBlock(result.output);
    } failureBlock:^(PWResult *result) {
        
        failureBlock(result);
    }];
    
    return request;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end

